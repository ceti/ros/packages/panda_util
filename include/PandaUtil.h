//
// Created by Johannes Mey on 12/10/21.
//

#ifndef DEMOPAPER_REAL_SRC_CCF_INCLUDE_CCF_UTIL_PANDAUTIL_H_
#define DEMOPAPER_REAL_SRC_CCF_INCLUDE_CCF_UTIL_PANDAUTIL_H_

class PandaUtil
{
public:
  /**
   * Recover the panda robot from any error states
   * The behavior is describe in https://frankaemika.github.io/docs/franka_ros.html#franka-control
   * @return true if the recovery succeeded
   */
  static bool recoverFromErrors();
};

#endif  // DEMOPAPER_REAL_SRC_CCF_INCLUDE_CCF_UTIL_PANDAUTIL_H_
